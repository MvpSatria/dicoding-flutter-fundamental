class TourismPlace {
  String name;
  String location;
  String description;
  String openDays;
  String openTime;
  String ticketPrice;
  String imageAsset;
  List<String> imageUrls;
 
  TourismPlace({
    required this.name,
    required this.location,
    required this.description,
    required this.openDays,
    required this.openTime,
    required this.ticketPrice,
    required this.imageAsset,
    required this.imageUrls,
  });
}

var tourismPlaceList = [
  TourismPlace(
    name: 'Handara Golf & Resort',
    location: 'Pancasari',
    description:
        'Handara Golf & Resort Bali is a mountain golf course & resort. It boasts a world class golf course in a magnificent setting, cozy hotel and cottage accommodation and a beautiful dining experience.',
    openDays: 'Open Everyday',
    openTime: '09:00 - 20:00',
    ticketPrice: 'Rp 25000',
    imageAsset: 'images/Bali-Handara-2.jpg',
    imageUrls: [
      'https://pix10.agoda.net/hotelImages/237994/-1/e454f3415c0316c36c9e883ee0e57434.jpg?ca=9&ce=1&s=1024x768',
      'https://cf.bstatic.com/xdata/images/hotel/max1024x768/269877982.jpg?k=450a99ae9a24f4f8ac04ae1addf89c4284cbf2315978b774a3a8699676c5013f&o=&hp=1',
      'https://www.handaragolfresort.com/wp-content/uploads/2017/11/tennis-1024x576.jpg?x16726'
    ],
  ),
  TourismPlace(
    name: 'Gunung Batur',
    location: 'Kintamani',
    description:
        'Konon menurut cerita dalam Lontar Susana Bali, Gunung Batur merupakan puncak dari Gunung Mahameru yang dipindahkan Batara Pasupati untuik dijadikan Sthana Betari Danuh (istana Dewi Danu).',
    openDays: 'Open Tuesday - Saturday',
    openTime: '24 hours',
    ticketPrice: 'Rp 20000',
    imageAsset: 'images/Gunung-Batur.jpg',
    imageUrls: [
      'https://asset.kompas.com/crops/3yY-5pIS9veuOAdSCk9yunt3Bdc=/0x0:780x520/750x500/data/photo/2021/08/04/610a5696664cf.png',
      'https://4.bp.blogspot.com/-wiUWrVtPEyY/WaLASKPYSdI/AAAAAAAAG9M/vpp5LFt4aFQNIvEabg_6r2dWYxMvb8wXwCLcBGAs/w1280-h720-p-k-no-nu/gunung%2Bbatur.jpg',
      'http://assets.kompasiana.com/items/album/2018/04/11/pemandangan-gunung-batur-png-5acda85acaf7db6ec05297a2.png',
    ],
  ),
  TourismPlace(
    name: 'Pantai Pandawa',
    location: 'Kuta Selatan',
    description:
        'Pandawa Beach mungkin masih terdengar asing dan tidak begitu terkenal untuk hari ini. Namun pantai ini memiliki sejuta pesona yang belum terpapar dan masih diperas oleh tebing batu kapur.',
    openDays: 'Open Everyday',
    openTime: '09:00 - 14:30',
    ticketPrice: 'Rp 20000',
    imageAsset: 'images/Pantai-Pandawa.jpg',
    imageUrls: [
      'https://s-light.tiket.photos/t/01E25EBZS3W0FY9GTG6C42E1SE/rsfit19201280gsm/events/2021/04/27/488a8381-63b9-473d-9cd1-ec9b1b63375d-1619502771730-1a0402162334124176d1087d09e32475.jpg',
      'https://www.pantainesia.com/wp-content/uploads/2018/05/panta-pandawa-1248x703.jpg',
      'https://pinhome-blog-assets-public.s3.amazonaws.com/2022/03/romitour-com.jpg'
    ],
  ),
  TourismPlace(
    name: 'Garuda Wisnu Kencana',
    location: 'Kuta Selatan',
    description:
        'Garuda, kata Garuda berasal dari bahasa Sanskerta yang berarti sebuah burung badanya seperti manusia, namun kepala dan kaki menyerupai burung serta di bagian punggung terdapat sayap. Tentunya burung Garuda adalah burung mitologi. Wisnu adalah salah satu dewa dalam Agama Hindu yang memiliki peranan dalam memelihara serta menjaga semua hasil ciptaan Tuhan. Sedangkan Kencana artinya Emas.',
    openDays: 'Open Everyday',
    openTime: '10:00 - 16:00',
    ticketPrice: 'Rp 20000',
    imageAsset: 'images/Garuda-Wisnu-Kencana.jpg',
    imageUrls: [
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7dc8Lfp7Yzz8ujMOGFuffT2FQeh3a4dLRFzFKdbS8bKUdiC1sHAV45wJT5SNj6WH7pwM&usqp=CAU',
      'https://ik.imagekit.io/tvlk/blog/2018/10/Uluwatu-Garuda-Wisnu-Kencana-Complete-1170326050.jpg',
      'https://jsp.co.id/wp-content/uploads/2018/08/cari-tempat-pelatihan-drone-terbaik.jpg'
    ],
  ),
  TourismPlace(
    name: 'Pantai Dreamland',
    location: 'Kuta Selatan',
    description:
        'antai Dreamland dikelilingi oleh tebing-tebing yang menjulang tinggi, dan dikelilingi batu karang yang lumayan besar di sekitar pantai.',
    openDays: 'Open Everyday',
    openTime: '09:00 - 17:00',
    ticketPrice: 'Rp 10000',
    imageAsset: 'images/Pantai-Dreamland.jpg',
    imageUrls: [
      'https://ik.imagekit.io/tvlk/blog/2020/01/Pantai_dreamland.jpg',
      'https://3.bp.blogspot.com/-Vws-Pd1ccD4/XGby4b320tI/AAAAAAAACNI/GTFqEzP70rg3mQ-AjMge5YaQKRWQibfrACLcBGAs/s1600/jejak-kenzie_pantai-dreamland-01.jpg',
      'https://www.pantainesia.com/wp-content/uploads/2018/08/pantai-dreamland.jpg'
    ],
  ),
];